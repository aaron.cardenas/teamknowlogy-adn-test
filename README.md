<!--
title: ' ADN Mutation Test whit Serverless Nodejs Rest API with TypeScript And MongoDB Atlas'
description: 'This is simple REST API example for AWS Lambda By Serverless framwork with TypeScript and MongoDB Atlas for ADN Mutation testing and stats.'
platform: AWS
language: nodeJS
authorName: 'Aarón Cárdenas'
-->

# Serverless Nodejs Rest API with TypeScript And MongoDB Atlas

This is simple REST API example for AWS Lambda By Serverless framwork with TypeScript and MongoDB Atlas.

## Use Cases

* Test ADN Mutation
* Get ADN Mutation stats
* REST API with typescript
* MongoDB Atlas data storage
* Multi-environment management under Serverless
* Mocha unit tests and lambda-tester interface test
* AWS lambda function log view

## Deploy

### To Test It Locally

* Run ```npm install``` to install all the necessary dependencies.
* Run ```npm run local``` use serverless offline to test locally. 

### Deploy on AWS, simply run:

```
$ npm run deploy

# or

$ serverless deploy

[Set AWS Credentials](https://www.serverless.com/framework/docs/providers/aws/guide/credentials/)
```

The expected result should be similar to:

```
Serverless: Compiling with Typescript...
Serverless: Using local tsconfig.json
Serverless: Typescript compiled.
Serverless: Packaging service...
Serverless: Excluding development dependencies...
Serverless: Uploading CloudFormation file to S3...
Serverless: Uploading artifacts...
Serverless: Uploading service teamknowlogy-adn-test.zip file to S3 (1.86 MB)...
Serverless: Validating template...
Serverless: Updating Stack...
Serverless: Checking Stack update progress...
......................................
Serverless: Stack update finished...
Service Information
service: teamknowlogy-adn-test
stage: dev
region: us-east-1
stack: teamknowlogy-adn-test-dev
resources: 32
api keys:
  None
endpoints:
  POST - https://xxxxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/adn/mutation
  GET - https://xxxxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/adn/stats

layers:
  None
Serverless: Removing old service artifacts from S3...
Serverless: Run the "serverless" command to setup monitoring, troubleshooting and testing.
```

## Usage

send an HTTP request directly to the endpoint using a tool like curl

```
POST | http://localhost:3000/dev/adn/mutation
GET  | http://localhost:3000/dev/adn/stats

     or 

curl https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/adn/mutation
curl https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/dev/adn/stats
```

## Scaling

By default, AWS Lambda limits the total concurrent executions across all functions within a given region to 100. The default limit is a safety limit that protects you from costs due to potential runaway or recursive functions during initial development and testing. To increase this limit above the default, follow the steps in [To request a limit increase for concurrent executions](http://docs.aws.amazon.com/lambda/latest/dg/concurrent-executions.html#increase-concurrent-executions-limit).
