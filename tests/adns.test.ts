import lambdaTester from 'lambda-tester';
import { expect } from 'chai';
import { mutation, stats } from '../app/handler';


describe('Stats [GET]', () => {
  it('success', () => {
    return lambdaTester(stats)
    .event({})
    .expectResult((result: any) => {
      expect(result.statusCode).to.equal(200);
      const body = JSON.parse(result.body);
      expect(body.code).to.equal(0);
    });
  });
 });

describe('Mutation [POST]', () => {
  it('success', () => {
    return lambdaTester(mutation)
      .event({ body: JSON.stringify({
        dna:["ATGCGG","CAGTGC","TTATGT","AGAAGT","CCCCTA","GCACTA"]
      })})
      .expectResult((result: any) => {
        expect(result.statusCode).to.equal(200);
        const body = JSON.parse(result.body);
        expect(body.code).to.equal(0);
      });
  });

  it('error', () => {
      return lambdaTester(mutation)
      .event({ body: JSON.stringify({
        dna:["ATGCGG","CAGTGC","TTATGT","AGAAGT","CCCCTA","GCACTAxxx"]
      })})
      .expectResult((result: any) => {
        expect(result.statusCode).to.equal(400);
        const body = JSON.parse(result.body);
        expect(body.code).to.equal(400);
      });
  });
  
  it('error', () => {
      return lambdaTester(mutation)
      .event({ body: JSON.stringify({
        dna:["CTGCGG","CAGTGC","TTATGT","AGAAGT","GCCCTA","GCACTA"]
      })})
      .expectResult((result: any) => {
        expect(result.statusCode).to.equal(403);
        const body = JSON.parse(result.body);
        expect(body.code).to.equal(403);
      });
  });
});
