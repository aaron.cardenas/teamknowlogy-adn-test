
export const castError = new Error('Cast to number failed for value "NaN" at path "id" for model "Books"');

export const stats = [
  {
    "dna":["ATGCGG","CAGTGC","TTATGT","AGAAGT","CCCCTA","GCACTA"]
  },
  {
    "dna":["ATGCGG","CAGTGC","TTATGT","AGAAGT","CCCCTA","GCACTA"]
  }
];

export const statsError = new Error('test find error');

export const mutation = {
  "dna":["ATGCGG","CAGTGC","TTATGT","AGAAGT","CCCCTA","GCACTA"]
}

export const mutationError = new Error('E11000 duplicate key error collection: study1.books index: id_1 dup key: { id: 30247892 }');

