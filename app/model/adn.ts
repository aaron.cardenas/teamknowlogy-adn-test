import mongoose from 'mongoose';

export type AdnDocument = mongoose.Document & {
  dna: string [],
  has_mutation?: boolean,
};

const adnSchema = new mongoose.Schema({
  dna: [String],
  signature: { type: String, index: true, unique: true, immutable: true, required: true },
  has_mutation: Boolean,
  createdAt: { type: Date, default: Date.now },
});

// Note: OverwriteModelError: Cannot overwrite `Adn` model once compiled. error
export const adn = (mongoose.models.adn ||
mongoose.model<AdnDocument>('adn', adnSchema, process.env.DB_ADNS_COLLECTION)
);