export class CreateAdnDTO {
  dna: string [];
  signature: string;
  has_mutation?: boolean;
}
