
import { Handler, Context } from 'aws-lambda';
import dotenv from 'dotenv';
import path from 'path';
const dotenvPath = path.join(__dirname, '../', `config/.env.${process.env.NODE_ENV}`);
dotenv.config({
  path: dotenvPath,
});

import { adn } from './model';
import { AdnController } from './controller/adn';
const adnController = new AdnController(adn);

export const mutation: Handler = (event: any, context: Context) => {
  return adnController.mutation(event, context);
};

export const stats: Handler = () => adnController.stats();
