import { Model } from 'mongoose';
import { CreateAdnDTO } from '../model/dto/createAdnDTO';

export class AdnService {
  private adn: Model < any > ;
  constructor(adn: Model < any > ) {
    this.adn = adn;
  }

  /**
   * Test ADN Mutation
   * Verify a dna matrix for mutations and format, and save a copy of the record and result if is new.
   * For better performance, the matches search stop if enough(requiredSequenceMatches) was found.
   * @param {CreateAdnDTO} params
   * @returns {String} Mutation confirmation
   */ 
  protected checkMutation(params: CreateAdnDTO): String {
    try {
      const requiredSequenceMatches = 2;
      const firstResults = this.verifyData(params.dna);
      let matches = firstResults.matches;
      params.signature = firstResults.fullSignature;

      
      matches += this.checkVerticalSequences(params.dna, matches, requiredSequenceMatches);
      matches += this.checkDiagonalSequences(params.dna, matches, requiredSequenceMatches);
      matches += this.checkBackwardDiagonalSequences(params.dna, matches, requiredSequenceMatches);
      
      params.has_mutation = matches >= requiredSequenceMatches;
      this.createAdn( params );
      if(!params.has_mutation){
        throw new Error ('No Mutation found!');
      }
      return 'Mutation found!';
    } catch (err) {
      console.error(err);
      throw err;
    }
  }

  /**
   * Check Sequence
   * Verify mutations in a single string
   * @param sequence string to check
   * @returns {number} number of mutations found
   */ 
  private checkSequence(sequence:string) : number{
    const sequenceMatches = sequence.toUpperCase().match(/A{4}|C{4}|G{4}|T{4}/g);
    return sequenceMatches ? sequenceMatches.length : 0;
  }

  /**
   * Verify Data
   * Verify a dna matrix format, and search mutations in horizontal sequences.
   * @param matrix
   * @returns {matches: number, fullSignatureny: string} Found matches and signatur to avoid duplicate db records.
   */ 
  private verifyData(matrix): any {
    let matches = 0;
    let fullSignature = "";
    for (let index = 0; index < matrix.length; index++) {
      fullSignature = fullSignature + matrix[index];
      let re = new RegExp('^(a|A|c|C|g|G|t|T){' + matrix.length + '}$');
      if (!re.test(matrix[index])) {
        console.error('The dna structure is invalid.')
        throw new Error ('The dna structure is invalid.');        
      } else {
        matches += this.checkSequence(matrix[index]);
      }
    }
    return {matches, fullSignature};
  }

  /**
   * Check Vertical Sequences
   * Stop if enough matches are found
   * @param matrix
   * @param currentMatches
   * @param requiredMatches
   * @returns {number} Found matches
   */
  private checkVerticalSequences(matrix, currentMatches, requiredMatches): any  {
    let matches = 0;
    const transposed_matrix = Object.assign([], matrix[0]).map((_, c) => [...matrix].map(r => r[c]));
    for (let index = 0; index < transposed_matrix.length; index++) {
      if (currentMatches + matches >= requiredMatches) {
        break;
      }
      matches += this.checkSequence(transposed_matrix[index].join(""));
    }
    return matches;
  }

  /**
   * Check Diagonal Sequences
   * Stop if enough matches are found
   * @param matrix
   * @param currentMatches
   * @param requiredMatches
   * @returns {number} Found matches
   */
  private checkDiagonalSequences(matrix, currentMatches, requiredMatches) {
    let matches = 0;
    for (let row = -(matrix.length - 4); row < matrix.length - 3; row++) {
      if (currentMatches + matches >= requiredMatches) {
        break;
      }
      let diagonal = [];
      for (let column = -(matrix.length - 4); column < matrix.length; column++) {
        if (column >= 0 && row + column < matrix.length && row + column >= 0) {
          diagonal.push(matrix[row + column][column]);
        }
      }
      matches += this.checkSequence(diagonal.join(""));
    }
    return matches;
  }

  /**
   * Check Backward Diagonal Sequences
   * Stop if enough matches are found
   * @param matrix
   * @param currentMatches
   * @param requiredMatches
   * @returns {number} Found matches
   */
  private checkBackwardDiagonalSequences(matrix, currentMatches, requiredMatches) {
    let matches = 0;
    let index = 0;
    for (let column = matrix.length * 2 - 5; column >= 3; column--) {
      if (currentMatches + matches >= requiredMatches) {
        break;
      }
      let diagonal = [];
      for (let row = 0; row < matrix.length; row++) {
        if (matrix.length * 2 - 5 - row - index >= 0 && matrix.length * 2 - 5 - row - index < matrix.length) {
          diagonal.push(matrix[row][matrix.length * 2 - 5 - row - index]);
        }
      }
      matches += this.checkSequence(diagonal.join(""));
      index++;
    }
    return matches;
  }

  /**
   * Create adn record
   * @param params
   * @returns {Promise < object >} Created record
   */
  private async createAdn(params: CreateAdnDTO): Promise < object > {
    try {
      const result = await this.adn.create({
        dna: params.dna,
        has_mutation: params.has_mutation,
        signature: params.signature,
      });

      return result;
    } catch (err) {
      console.error(err);
    }
  }
  
  /**
   * Get mutation stats
   * @returns {number, number, number} Mutations count, No mutation count, Ratio.
   */
   protected async adnStats() {
    const data = await this.adn.find({}, 'has_mutation') as any;
    const total = data.length;
    const mutations = data.filter(record => record.has_mutation).length;
    const noMutations = total - mutations;
    return  {"count_mutations":mutations, "count_no_mutation":noMutations, "ratio":mutations/noMutations};
  }
}
