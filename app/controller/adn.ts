import { Context } from 'aws-lambda';
import { Model } from 'mongoose';
import { MessageUtil } from '../utils/message';
import { AdnService } from '../service/adn';
import { CreateAdnDTO } from '../model/dto/createAdnDTO';

export class AdnController extends AdnService {
  constructor (adn: Model<any>) {
    super(adn);
  }

  /**
   * Test ADN Mutation
   * @param {*} event
   */
  async mutation (event: any, context?: Context) {
    console.log('functionName', context.functionName);
    const params: CreateAdnDTO = JSON.parse(event.body);

    try {
      const result = await this.checkMutation({
        dna: params.dna,
        signature: ""
      });

      return MessageUtil.success(result);
    } catch (err) {
      console.error(err);
      switch ( err.message) {
        case 'The dna structure is invalid.':
          return MessageUtil.error(400, err.message, 400);
        case 'No Mutation found!':
          return MessageUtil.error(403, err.message, 403);
        default:
          return MessageUtil.error(err.code, err.message, 500);
      }      
    }
  }
  
  /**
   * ADN Mutation Stats
   */
  async stats ( ) {
    try {
      const result = await this.adnStats( );

      return MessageUtil.success(result);
    } catch (err) {
      console.error(err);
      return MessageUtil.error(err.code, err.message, 500);
    }
  }
}
